pt-BR:

O emulador de computadores BeebEm foi criado por Dave Gilbert, segundo as informações contidas na página eletrônica http://www.mkw.me.uk/beebem/. Emula os microcomputadores BBC Micro e Master 128 que permite executar os programas aplicativos e jogos do BBC Micro no seu computador.

Contém: o arquivo ou pacote de instalação .deb e um arquivo de texto em idioma "pt-BR" explicando como utilizá-lo.

Transfira ou baixe ou descarregue os arquivos "beebem_0.0.13-1_amd64.deb", "beebem_0.0.13-1_amd64.deb.md5.sum", "beebem_0.0.13-1_amd64.deb.sha256.sum" e o arquivo de texto ".txt" que estão no arquivo compactado ".zip"

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Der Computeremulator BeebEm wurde von Dave Gilbert erstellt, gemäß den Informationen auf der Website http://www.mkw.me.uk/beebem/. Es emuliert die Mikrocomputer BBC Micro und Master 128, mit denen Sie Anwendungsprogramme und Spiele von BBC Micro auf Ihrem Computer ausführen können.

Es enthält: die .deb-Installationsdatei oder das Paket und eine Textdatei in "pt-BR"-Sprache, die erklärt, wie man es benutzt.

Laden Sie die Dateien „beebem_0.0.13-1_amd64.deb“, „beebem_0.0.13-1_amd64.deb.md5.sum", „beebem_0.0.13-1_amd64.deb.sha256.sum“ und die Textdatei „.txt“ herunter in der komprimierten „.zip“-Datei.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

The BeebEm computer emulator was created by Dave Gilbert, according to information contained on the website http://www.mkw.me.uk/beebem/. It emulates the BBC Micro and Master 128 microcomputers that allow you to run BBC Micro application programs and games on your computer.

Contains: the .deb installation file or package and a text file in "pt-BR" language explaining how to use it.

Download the files "beebem_0.0.13-1_amd64.deb", "beebem_0.0.13-1_amd64.deb.md5.sum", "beebem_0.0.13-1_amd64.deb.sha256.sum" and the text file ".txt" and the text file " .txt" that are in the compressed file ".zip".

All credits and rights are included in the files, in respect of the volunteer work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

es:

El emulador de computadora BeebEm fue creado por Dave Gilbert, según la información contenida en el sitio web http://www.mkw.me.uk/beebem/. Emula las microcomputadoras BBC Micro y Master 128 que le permiten ejecutar programas y juegos de aplicaciones BBC Micro en su computadora.

Contiene: el archivo o paquete de instalación .deb y un archivo de texto en idioma "pt-BR" que explica cómo usarlo.

Descargue los archivos "beebem_0.0.13-1_amd64.deb", "beebem_0.0.13-1_amd64.deb.md5.sum", "beebem_0.0.13-1_amd64.deb.sha256.sum" y el archivo de texto ".txt" que se encuentran en el archivo comprimido ".zip".

Todos los créditos y derechos están incluidos en los archivos, en relación con el trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

fr :

L'émulateur informatique BeebEm a été créé par Dave Gilbert, selon les informations contenues sur le site http://www.mkw.me.uk/beebem/. Il émule les micro-ordinateurs BBC Micro et Master 128 qui vous permettent d'exécuter des programmes d'application et des jeux BBC Micro sur votre ordinateur.

Contient : le fichier ou package d'installation .deb et un fichier texte en "pt-BR" expliquant comment l'utiliser.

Téléchargez les fichiers "beebem_0.0.13-1_amd64.deb", "beebem_0.0.13-1_amd64.deb.md5.sum", "beebem_0.0.13-1_amd64.deb.sha256.sum" et le fichier texte ".txt" qui se trouvent dans le fichier compressé ".zip".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

L'emulatore di computer BeebEm è stato creato da Dave Gilbert, secondo le informazioni contenute nel sito Web http://www.mkw.me.uk/beebem/. Emula i microcomputer BBC Micro e Master 128 che consentono di eseguire programmi applicativi e giochi BBC Micro sul computer.

Contiene: il file o pacchetto di installazione .deb e un file di testo in lingua "pt-BR" che spiega come utilizzarlo.

Scarica i file "beebem_0.0.13-1_amd64.deb", "beebem_0.0.13-1_amd64.deb.md5.sum", "beebem_0.0.13-1_amd64.deb.sha256.sum" e il file di testo ".txt" che si trovano nel file compresso ".zip".

Tutti i crediti e i diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
